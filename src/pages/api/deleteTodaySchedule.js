import database from "./database";

const deleteTodaySchedule = async (req, res) => {
  try {
    let scheduleId = req.body.scheduleId;

    const { rows } = await database.query(
      `DELETE FROM schedule WHERE schedule_id = $1`,
      [scheduleId],
    );

    res.status(200).send(scheduleId);
  } catch (error) {
    res.status(500).send({ error: error.message });
  };
};

export default deleteTodaySchedule;