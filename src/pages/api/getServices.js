import database from "./database";

const getServices = async (req, res) => {
  try {
    const { rows } = await database.query(
      `SELECT * FROM wash_type`,
    );

    res.status(200).send(rows);
  } catch (error) {
    res.status(500).send({ error: error.message });
  };
};

export default getServices;