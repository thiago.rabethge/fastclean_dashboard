import moment from "moment";
import database from "./database";

const getTodaySchedules = async (req, res) => {
  try {
    let currentDate = moment(new Date()).format("DD/MM/YYYY");

    const {rows} = await database.query(
      `SELECT * FROM schedule WHERE reserved = true AND date = $1 ORDER BY time`,
      [currentDate],
    );

    res.status(200).send(rows);
  } catch (error) {
    res.status(500).send({error: error.message});
  };
};

export default getTodaySchedules;