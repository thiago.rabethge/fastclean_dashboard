import { create } from 'zustand';

const useTodaySchedulesStore = create((set) => ({
  todaySchedulesList: [],
  setTodaySchedulesList: (newTodaySchedulesList) => set(() => ({ todaySchedulesList: newTodaySchedulesList })),
}));

export default useTodaySchedulesStore;