import { create } from 'zustand';

const useServicesStore = create((set) => ({
    servicesList: [],
    setServicesList: (newServicesList) => set(() => ({ servicesList: newServicesList })),
}));

export default useServicesStore;