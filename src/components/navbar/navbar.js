import Link from "next/link";

const Navbar = () => {
  return (
    <ul className="nav nav-tabs mt-3 mb-3 justify-content-center">
      <li className="nav-item">
        <Link href="/" className="nav-link active">
          Agenda do dia
        </Link>
      </li>
      {/* <li class="nav-item">
        <Link href="/" className="nav-link">
          Painel de serviços
        </Link>
      </li> */}
    </ul>
  );
};

export default Navbar;