'use client'
import Navbar from "@/components/navbar/navbar";
import getServices from "@/requests/services/getServices";
import deleteTodaySchedule from "@/requests/todaySchedules/deleteTodaySchedule";
import getTodaySchedules from "@/requests/todaySchedules/getTodaySchedules";
import useServicesStore from "@/stores/servicesStore";
import useTodaySchedulesStore from "@/stores/todaySchedulesStore";
import 'bootstrap/dist/css/bootstrap.css';
import { useMemo } from "react";
import Swal from "sweetalert2";

const Home = () => {
  const { todaySchedulesList, setTodaySchedulesList } = useTodaySchedulesStore();
  const { servicesList, setServicesList } = useServicesStore();

  const GetTodaySchedules = () => {
    getTodaySchedules()
      .then((response) => setTodaySchedulesList(response.data));
  };

  const GetServices = () => {
    getServices()
      .then((response) => setServicesList(response.data));
  };

  useMemo(() => {
    GetTodaySchedules();
    GetServices();
  }, []);

  const handleConcludeSchedule = (scheduleId) => {
    deleteTodaySchedule(scheduleId)
      .then(() => {
        Swal.fire(
          'Atendimento',
          'Atendimento finalizado',
          'success'
        )
        GetTodaySchedules();
      });
  };

  return (
    <div className="container">
      <Navbar />

      <div className="table-responsive">
        <table className="table table-hover">
          <thead>
            <tr>
              <th>
                Data
              </th>
              <th>
                Hora
              </th>
              <th>
                Modelo de carro
              </th>
              <th>
                Tipo de lavagem
              </th>
              <th>
              </th>
            </tr>
          </thead>
          <tbody>
            {todaySchedulesList.map((todaySchedule) => {
              return (
                <tr key={todaySchedule.schedule_id}>
                  <td>
                    {todaySchedule.date}
                  </td>
                  <td>
                    {todaySchedule.time}
                  </td>
                  <td>
                    {todaySchedule.car_model}
                  </td>
                  {servicesList.map((service) => (
                    service.wash_id == todaySchedule.wash_type_id &&
                    <td key={service.wash_id}>
                      {service.name}
                    </td>
                  ))}
                  <td>
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={() => handleConcludeSchedule(todaySchedule.schedule_id)}
                    >
                      Finalizar atendimento
                    </button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default Home;