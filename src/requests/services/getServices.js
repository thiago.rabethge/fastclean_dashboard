import api from "@/services/api";

const getServices = () => {
  return (
    api
      .get("/api/getServices")
      .then((response) => response)
      .catch((error) => console.error(error))
  );
};

export default getServices;