import api from "../../services/api";

const getTodaySchedules = () => {
  return (
    api
      .get("/api/getTodaySchedules")
      .then((response) => response)
      .catch((error) => console.error({ error: error.message }))
  );
};

export default getTodaySchedules;