import api from "@/services/api";

const deleteTodaySchedule = (scheduleId) => {
  return (
    api
      .post("/api/deleteTodaySchedule", {
        scheduleId: scheduleId,
      })
      .then((response) => response)
      .catch((error) => console.error(error))
  );
};

export default deleteTodaySchedule;